import React from "react"
import { View, Text } from "react-native"

import { Strings } from "./../resources/index"
import styles from "./../styles"
import { getFormattedDate } from "./../utils/DateUtils"

type Props = {
	transaction: any
}

export const DetailsItem = (props: Props) => {

	const { transaction } = props
	const formattedPurchaseDateTime = getFormattedDate(transaction.purchaseTime)
	
	return (
		<View style={styles.cardItemContainer}>
			<Text style={styles.detailsTitle}>{Strings.DETAILS}</Text>
			<View style={styles.itemDetailsLine}>
				<Text style={styles.descriptionText}>{Strings.PURCHASED_AT}</Text>
				<Text style={[styles.descriptionText, styles.detailsDescription]}>{formattedPurchaseDateTime}</Text>
			</View>
			<View style={styles.itemDetailsLine}>
				<Text style={styles.descriptionText}>{Strings.MERCHANT_NAME}</Text>
				<Text style={[styles.descriptionText, styles.detailsDescription]}>{transaction.merchant.name}</Text>
			</View>
			<View style={styles.itemDetailsLine}>
				<Text style={styles.descriptionText}>{Strings.WEBSITE}</Text>
				<Text style={[styles.descriptionText, styles.detailsHighlight]}>{transaction.merchant.website}</Text>
			</View>
		</View>
	)
}