export function getFormattedDate(transactionDateTime: string) {
	const purchaseDateTime = new Date(transactionDateTime)

	//This formatiing could also be done with the moment.js library
	const month = purchaseDateTime.toLocaleString('default', { month: 'short' })
	const day = purchaseDateTime.getDay()
	const time = purchaseDateTime.toLocaleTimeString([], { hour: 'numeric', minute: 'numeric', hour12: true })
	
	return month + " " + day + ", " + time
}